package ntt.loopviewpagerexample.adapters;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ntt.loopviewpagerexample.utils.Utils;

public class CustomPagerAdapter extends PagerAdapter {

    private ArrayList<String> mListImagePath;

    public CustomPagerAdapter(ArrayList<String> listImagePath) {
        this.mListImagePath = listImagePath;
    }

    @Override
    public int getCount() {
        return mListImagePath.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        ImageView img = new ImageView(view.getContext());
        img.setBackgroundColor(Utils.getColor(view.getContext(), android.R.color.white));
        Picasso.get().load(mListImagePath.get(position)).into(img);
        view.addView(img, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        return img;
    }

}
