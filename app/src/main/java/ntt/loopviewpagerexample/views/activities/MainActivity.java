package ntt.loopviewpagerexample.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import ntt.loopviewpagerexample.R;
import ntt.loopviewpagerexample.adapters.CustomPagerAdapter;
import ntt.loopviewpagerexample.views.customs.LoopViewPager;

public class MainActivity extends AppCompatActivity {

    private LoopViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    private CustomPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }

    private void initView() {
        mViewPager = findViewById(R.id.viewpager);
        mCircleIndicator = findViewById(R.id.indicator);
    }

    private void initData() {
        ArrayList<String> listImagePath = new ArrayList<>();

        listImagePath.add("http://i.imgur.com/DvpvklR.png");
        listImagePath.add("https://www.christies.com/media-library/images/features/articles/2017/05/05/pablo-picasso-femme-assise-robe-bleue/pablo-picasso-femme-assise-robe-bleue.jpg?w=780");
        listImagePath.add("https://www.faguowenhua.com/imaginary/fit?filename&height=600&type=webp&url=http%3A%2F%2Fapp%2Fsites%2Fdefault%2Ffiles%2F2018-07%2Fpablo-picasso-buste-de-femme-20-may-1938.jpg&width=800");

        mAdapter = new CustomPagerAdapter(listImagePath);

        mViewPager.setAdapter(mAdapter);
        mCircleIndicator.setViewPager(mViewPager);
    }
}
